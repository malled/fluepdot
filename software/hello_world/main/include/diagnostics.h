#pragma once

/**
 * Displays diagnostics for a flipnet interface passed as pvParam
 */
void interface_diagnostics_task(void* pvParam);
